# GPS Tracker Server

## Socket Server
  Working on st901 adapter
  
## Web Server
  Blank

### H02 Protocol
  
  There are 2 structures of commands send by terminal
  
  - Normal information(**V1**): \*XX,YYYYYYYYYY,V1,HHMMSS,S,latitude,D,longitude,G,speed,direction,DDMMYY,vehicle_status\#
  - Confirmation of information(**V4**): \*XX,YYYYYYYYYY,V4,CMD,hhmmss,HHMMSS,S,latitude,D,longitude,G,speed,direction,DDMMYY,vehicle_status\#

  In which:
  
  - **\***: Head of command.
  - **XX**: Name of maker, consists.
  - **YYYYYYYYYY** : SN of terminal, is ten characters front of IMEI.
  - **CMD**: Command.
  - **HHMMSS**: Time: hour/minute/second,device time, GMT, Beijing is 8 hours ahead GMT.
  - **S**: Effective mark of data, ‘A’ stand for effective, ‘V’ stand for invalid. 
  - **Latitude**: Latitude, format: **DDFF.FFFF**, **DD**: Degrees(00 ~ 90), **FF.FFFF**: Minutes(00.0000 ~ 59.9999), keep four decimal places. 
  - **D**: Latitude marks (N:north, S:south).
  - **Longitude** : longitude, format: **DDDFF.FFFF**, **DDD**: Degrees(000 ~ 180), **FF.FFFF**: Minutes(00.0000 ~ 59.9999), keep four decimal places. 
  - **G**: longitude marks (**E**: east, **W**: west).
  - **Speed**: speed,range of 000.00 ~ 999.99 knots, Keep two decimal places. If speed is empty then its 0.
  - **Direction**: Azimuth, north to 0 degrees, resolution 1 degrees, clockwise direction. If direction is empty then its 0.
  - **Vehicle_status**: Used for alarms (yet to be understood)

# Google Cloud
  **All Mongo Info**
  - https://docs.mongodb.com/manual/tutorial/install-mongodb-on-debian/

  **How to access through SFTP**
  - https://pietti.com/como-acceder-por-ftp-a-google-cloud-con-filezilla/#otorgar-permisos-de-escritura
  
  **Node processes commands, list, kill id, kill all**
  - ps -aef | grep node
  - kill -9 XXXX
  - pkill node

  **How to make routing available in Google Cloud**
  - Add .htaccess file in website folder
  ```
    RewriteEngine On
    # If an existing asset or directory is requested go to it as it is
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -f [OR]
    RewriteCond %{DOCUMENT_ROOT}%{REQUEST_URI} -d
    RewriteRule ^ - [L]

    # If the requested resource doesn't exist, use index.html
    RewriteRule ^ /index.html
  ```
  - then go to google cloud server through SSH connection and execute the following command:
  ```sudo a2enmod rewrite```
  - edit the file /etc/apache2/sites-available/000-default.conf adding the next lines after 'DocumentRoot /var/html/www'
  ```
  <Directory /var/www/html/>
        Options Indexes FollowSymLinks
        AllowOverride None
        Require all granted
  </Directory>
  ```
  - finally restart apache service
  ```sudo service apache2 restart```