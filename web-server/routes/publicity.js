const { update } = require('../models/publicity');

const express = require('express'),
  router = express.Router(),
  Publicity = require('../models/publicity'),
  Bus = require('../models/bus'),
  multer = require('multer'),
  fs = require('fs');

const storage = multer.diskStorage({
  destination: (req, file, callback) => {
    callback(null, 'uploads/publicity')
  },
  filename: (req, file, callback) => {
    let filename = "";
    if (file.originalname == null || !file.originalname.length || !file.originalname.split(".")[0].length)
      filename = "noname";
    else
      filename = (file.originalname.split(".")[0]) + '-' + Date.now() + '.jpeg';
    callback(null, filename);
  }
})
const uploadFile = multer({ storage: storage }).fields([{ name: 'img', maxCount: 1 }]);

//Get all publicity
router.get('/', (req, res, next) => {
  let findObject = {};
  if (req.query.routeId)
    findObject.routes = req.query.routeId;

  Publicity.find(findObject)
    .populate('routes')
    .populate('activity.user')
    .sort({ date: -1 })
    .then(publicity => {
      res.json(publicity);
    }).catch(next);
});

router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  Publicity.find(dataToFind)
    .populate('routes')
    .populate('activity.user')
    .sort({ date: -1 })
    .then(publicity => {
      res.json(publicity);
    }).catch(next);
});

router.post('/findByRoute', (req, res, next) => {
  let manageResults = (publicity) => {
    if (publicity.length > 1) {
      res.json(publicity[Math.floor(Math.random() * publicity.length)]);
    } else if (publicity.length == 1) {
      res.json(publicity[0]);
    } else {
      res.json(null);
    }
  }

  let findAllRoutes = () => {
    Publicity.find({ allRoutes: true })
      .populate('routes')
      .populate('activity.user')
      .then(publicity => {
        manageResults(publicity);
      }).catch(next);
  }

  let routeId = req.body.routeId;
  if (routeId) {
    Publicity.find({ routes: routeId })
      .populate('routes')
      .populate('activity.user')
      .sort({ date: -1 })
      .then(publicity => {
        if (publicity.length)
          manageResults(publicity);
        else
          findAllRoutes();
      }).catch(next);
  } else {
    findAllRoutes();
  }

});

//Create publicity
router.post('/', uploadFile, (req, res, next) => {
  delete req.body._id;
  if (req.files == null || req.files['img'] == null || req.files['img'][0] == null) {
    res.status(422).json({ customErrorMessage: "Campo img necesario" });
    return;
  }
  if (req.body.user == null) {
    res.status(422).json({ customErrorMessage: "Campo usuario necesario" });
    return;
  }

  let body = {
    title: req.body.title,
    description: req.body.description,
    imgName: req.files['img'][0].filename,
    allRoutes: req.body.allRoutes,
    routes: req.body.routes,
    active: req.body.active,
    activity: [{
      description: "Registro creado",
      user: req.body.user
    }],
  };

  Publicity.create(body).then(createdPublicity => {
    res.json(createdPublicity);
  }).catch(next);

});

//Create publicity
router.put('/', uploadFile, (req, res, next) => {
  delete req.body._id;
  let replaceFile = false;
  if (req.files != null && req.files['img'] != null && req.files['img'][0] != null)
    replaceFile = true;
  if (req.body.user == null) {
    res.status(422).json({ customErrorMessage: "Campo 'user' necesario" });
    return;
  }
  else if (req.body.description == null) {
    res.status(422).json({ customErrorMessage: "Campo 'description' necesario" });
    return;
  }

  Publicity.findOne({ _id: req.body.id }).then(publicity => {
    var previousImgName = publicity.imgName;
    if (replaceFile) {
      publicity.imgName = req.files['img'][0].filename;
      req.body.replaceFile = true;
    }

    let activityDescription = getChangesString(publicity, req.body);
    if (activityDescription != null) {
      publicity.activity.push({
        description: activityDescription,
        user: req.body.user
      });
    }

    publicity.title = req.body.title;
    publicity.description = req.body.description;
    publicity.allRoutes = req.body.allRoutes;
    publicity.routes = req.body.routes;
    publicity.active = req.body.active;

    Publicity.findOneAndUpdate({ _id: publicity._id }, publicity, { new: true })
      .populate('routes')
      .populate('activity.user')
      .then(updatedPublicity => {
        if (replaceFile)
          deleteFile(previousImgName);

        res.json(updatedPublicity);
      });
  }).catch((err) => {
    if (replaceFile)
      deleteFile(publicity.imgName);
    res.status(422).json({ customErrorMessage: "Registro de publicidad no encontrado" });
  });
});

function getChangesString(original, updated) {

  let changes = [];
  if (original.title != updated.title)
    changes.push("titulo");
  if (original.description != updated.description)
    changes.push("descripción");
  if (parseToBoolean(original.allRoutes) != parseToBoolean(updated.allRoutes) || (updated.routes != null && original.routes.length != updated.routes.length))
    changes.push("configuración de rutas");
  if (parseToBoolean(original.active) != parseToBoolean(updated.active))
    changes.push("estatus");
  if (updated.replaceFile)
    changes.push("archivo multimedia");

  if (!changes.length)
    return null;
  if (changes.length == 1)
    return "Cambios en " + changes[0];
  else if (changes.length == 2)
    return "Cambios en " + changes[0] + " y " + changes[1];
  else if (changes.length > 2) {
    let changesString = "Cambios en ";
    for (let i = 0; i < changes.length; i++) {
      if (i + 2 == changes.length)
        changesString += changes[i] + " y " + changes[i + 1];
      else
        changesString += changes[i] + ", ";
    }
    return changesString;
  }
}

function deleteFile(fileName) {
  return new Promise((resolve, reject) => {
    fs.unlink('uploads/publicity/' + fileName, (err) => {
      resolve(err);
    });
  })
}

function parseToBoolean(a) {
  if (typeof a == "boolean")
    return a;
  else if (typeof a == "string") {
    if (a == "false" || a == "0")
      return false;
    else
      return Boolean(a)
  } else {
    return Boolean(a);
  }
}

module.exports = router;
