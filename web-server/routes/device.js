const router = require('express').Router(),
  Device = require('../models/device');


router.get('/', (req, res, next) => {
  Device.find().then(devices => {
    res.json(devices);
  }).catch(next);
});

router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  Device.find(dataToFind).then(devices => {
    res.json(devices);
  }).catch(next);

});

//Create route
router.post('/', (req, res, next) => {

  delete req.body._id;
  Device.create(req.body).then(newDevice => {
    res.json(newDevice);
  }).catch(next);
});

//Update route
router.put('/', (req, res, next) => {
  Device.findByIdAndUpdate({ _id: req.body._id }, req.body, { new: true }).then((updatedDevice) => {
    res.json(updatedDevice);
  });

});
module.exports = router;
