const express = require('express'),
  router = express.Router(),
  Report = require('../models/report'),
  Bus = require('../models/bus'),
  multer = require('multer');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'uploads/reports')
  },
  filename: (req, file, cb) => {
    cb(null, (file.originalname) + '-' + Date.now() + '.jpeg')
  }
})
const uploadFile = multer({ storage: storage }).fields([{ name: 'img', maxCount: 1 }]);

//Get all reports
router.get('/', (req, res, next) => {
  Report.find()
    .populate('bus')
    .populate('route')
    .sort({ date: -1 })
    .then(reports => {
      res.json(reports);
    }).catch(next);
});

router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  Report.find(dataToFind)
    .populate('bus')
    .populate('route')
    .then(reports => {
      res.json(reports);
    }).catch(next);
});

//Create report
router.post('/', uploadFile, (req, res, next) => {
  delete req.body._id;
  if (req.files == null || req.files['img'] == null || req.files['img'][0] == null) {
    res.status(422).json({ customErrorMessage: "Campo img necesario" });
    return;
  }

  let errors = getErrors(req.body);
  if (errors != null) {
    res.status(422).json({ customErrorMessage: errors });
    return;
  }

  Bus.findOne({ _id: req.body.bus }).then(busData => {
    return {
      title: req.body.title,
      tickerNumber: req.body.tickerNumber,
      bus: busData._id,
      route: busData.route,
      phoneId: req.body.phoneId,
      description: req.body.description,
      contactPhoneNumber: req.body.contactPhoneNumber,
      imgName: req.files['img'][0].filename,
      response: null
    };
  }).then(parsedBody => {
    return Report.create(parsedBody);
  }).then(createdReport => {
    res.json(createdReport);
  }).catch(next);

});

router.post('/respond', (req, res, next) => {
  Report.findOneAndUpdate({ _id: req.body.reportId }, { response: { text: req.body.response, date: Date.now() } }, { new: true })
    .populate('bus')
    .populate('route')
    .then(updatedReport => {
      res.json(updatedReport);
    }).catch(next);
});

function getErrors(body, newRecord) {
  let errorList = [];
  if (body.phoneId == null)
    errorList.push("falta campo phoneId");
  else if (typeof body.phoneId != "string" || !body.phoneId.length)
    errorList.push("campo phoneId invalido");
  if (body.description == null)
    errorList.push("falta campo descripción");
  else if (typeof body.description != "string" || !body.description.length)
    errorList.push("campo descripción invalido");

  if (!errorList.length)
    return null;
  else
    return "Errores: " + errorList.join(", ");
}

module.exports = router;
