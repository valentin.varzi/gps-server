var express = require('express');
var router = express.Router();
var User = require('../models/user');

//Get all routes
router.get('/', (req, res, next) => {
  User.find().populate('permissions.permission').then(users => {
    res.json(users);
  }).catch(next);
});


router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  User.find(dataToFind).then(users => {
    res.json(users);
  }).catch(next);

});

//Create route
router.post('/', (req, res, next) => {

  delete req.body._id;
  User.create(req.body).then(newUser => {
    res.json(newUser);
  }).catch(next);

});

//Update route
router.put('/', (req, res, next) => {

  User.findByIdAndUpdate({ _id: req.body._id }, req.body).then(() => {
    User.findOne({ _id: req.body._id }).then(updatedUser => {
      res.json(updatedUser);
    });
  });

});

//Delete route
router.delete('/:id', (req, res, next) => {

  User.findOneAndDelete({ _id: req.params.id }).then(deletedUser => {
    res.json(deletedUser);
  }).catch(next);

});

module.exports = router;
