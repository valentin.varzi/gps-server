const router = require('express').Router(),
  User = require('../models/user'),
  backup = require('mongodb-snapshot');

router.post('/', (req, res, next) => {
  let loginData = {
    username: req.body.username,
    password: req.body.password
  };

  User.findOne(loginData).populate('permissions.permission').then(user => {
    res.json(user);
  }).catch(next);
});

router.get('/dbBackup', async (req, res, next) => {
  const mongo_connector = new backup.MongoDBDuplexConnector({
    connection: {
      uri: `mongodb://localhost/Atusum`,
      dbname: 'Atusum',
    },
  });

  const localfile_connector = new backup.LocalFileSystemDuplexConnector({
    connection: {
      path: './backups/backup.tar',
    },
  });

  const transferer = new backup.MongoTransferer({
    source: mongo_connector,
    targets: [localfile_connector],
  });

  for await (const { total, write } of transferer) {
    console.log(`remaining bytes to write: ${total - write}`);
  }

  const file = `./backups/backup.tar`;
  res.download(file); // Set disposition and send it.
});

router.get('/restoreDB', async (req, res, next) => {

  const mongo_connector = new backup.MongoDBDuplexConnector({
    connection: {
      uri: `mongodb://localhost/Atusum2`,
      dbname: 'Atusum2',
    },
  });

  const localfile_connector = new backup.LocalFileSystemDuplexConnector({
    connection: {
      path: './backups/backup2.tar',
    },
  });

  const transferer = new backup.MongoTransferer({
    source: localfile_connector,
    targets: [mongo_connector],
  });

  for await (const { total, write } of transferer) {
    console.log(`remaining bytes to write: ${total - write}`);
  }
  res.json(true);
});


module.exports = router;
