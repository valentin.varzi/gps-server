const Bus = require('../models/bus');

const router = require('express').Router(),
  PositionHistory = require('../models/positionHistory');


router.get('/', (req, res, next) => {
  PositionHistory.find().then(allData => {
    res.json(allData);
  }).catch(next);
});

router.get('/query', (req, res, next) => {
  PositionHistory.find(req.body)
    .populate('bus')
    .populate('route')
    .then(data => {
      res.json(data);
    }).catch(next);
});

router.get('/activeBuses', (req, res, next) => {
  let anHourAgo = new Date(Date.now() - 1 * 60 * 60 * 1000);
  let searchParams = {
    date: {
      $gte: anHourAgo
    }
  };

  PositionHistory.find(searchParams).distinct('bus').then(data => {
    res.json(data);
  }).catch(next);
});

router.get('/busLastPosition/:busId', (req, res, next) => {
  PositionHistory.findOne({ bus: req.params.busId, date: { $lt: new Date(2025, 01, 01) } }).sort({ date: -1 }).exec((err, data) => {
    res.json(data);
  });
})

router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  let searchParams = {
    date: {
      $gte: new Date(dataToFind.startDate),
      $lte: new Date(dataToFind.endDate)
    }
  };

  if (dataToFind.bus)
    searchParams.bus = dataToFind.bus;
  if (dataToFind.route)
    searchParams.route = dataToFind.route;

  console.log(searchParams);
  if (searchParams.bus == null) {
    res.json([]);
    return;
  }

  PositionHistory.find(searchParams)
    .populate('bus')
    .populate('route')
    .sort({ date: 1 })
    .then(positions => {
      res.json(positions);
    }).catch(next);
});

//Create route
router.post('/', (req, res, next) => {

  delete req.body._id;
  PositionHistory.create(req.body).then(newDevice => {
    res.json(newDevice);
  }).catch(next);
});

module.exports = router;
