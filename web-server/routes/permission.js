const router = require('express').Router(),
  Permission = require('../models/permission');


router.get('/', (req, res, next) => {
  Permission.find().then(permissions => {
    res.json(permissions);
  }).catch(next);
});

router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  Permission.find(dataToFind).then(permissions => {
    res.json(permissions);
  }).catch(next);

});

router.post('/', (req, res, next) => {

  delete req.body._id;
  Permission.create(req.body).then(newPermission => {
    res.json(newPermission);
  }).catch(next);
});

router.put('/', (req, res, next) => {
  Permission.findByIdAndUpdate({ _id: req.body._id }, req.body, { new: true }).then((updatedPermission) => {
    res.json(updatedPermission);
  });

});
module.exports = router;
