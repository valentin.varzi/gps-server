const deviceRouter = require('./device'),
  busRouter = require('./bus'),
  userRouter = require('./user'),
  routeRouter = require('./route'),
  securityRouter = require('./security'),
  positionHistory = require('./positionHistory'),
  report = require('./report'),
  publicity = require('./publicity'),
  permission = require('./permission');

module.exports = [
  { path: '/device', module: deviceRouter },
  { path: '/bus', module: busRouter },
  { path: '/user', module: userRouter },
  { path: '/route', module: routeRouter },
  { path: '/security', module: securityRouter },
  { path: '/history', module: positionHistory },
  { path: '/report', module: report },
  { path: '/publicity', module: publicity },
  { path: '/permission', module: permission },
];