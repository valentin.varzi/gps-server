const express = require('express'),
  router = express.Router(),
  Route = require('../models/route'),
  Bus = require('../models/bus'),
  multer = require("multer"),
  upload = multer(),
  xlsx = require('node-xlsx');

//Get all routes
router.get('/', (req, res, next) => {
  Route.find().then(routees => {
    res.json(routees);
  }).catch(next);
});


router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  Route.find(dataToFind).then(routes => {
    res.json(routes);
  }).catch(next);

});

//Create route
router.post('/', (req, res, next) => {

  delete req.body._id;
  Route.create(req.body).then(newRoute => {
    res.json(newRoute);
  }).catch(next);

});

//Update route
router.put('/', (req, res, next) => {

  Route.findByIdAndUpdate({ _id: req.body._id }, req.body).then(() => {
    Route.findOne({ _id: req.body._id }).then(updatedRoute => {
      res.json(updatedRoute);
    });
  });

});

//Delete route
router.delete('/:id', (req, res, next) => {

  Route.findOneAndDelete({ _id: req.params.id }).then(deletedRoute => {
    res.json(deletedRoute);
  }).catch(next);

});

//Update routes by file
router.post('/updateWithFile', upload.single('rolRutas'), async (req, res, next) => {
  let excelFile = xlsx.parse(req.file.buffer);
  let excelRows = excelFile.length ? (excelFile[0].data || []) : [];
  let parsedRows = excelRows.map(r => {
    return {
      busNumber: r[1],
      routeName: r[2],
      driverName: r[3]
    };
  });

  let allRoutes = await Route.find();
  let allBuses = await Bus.find();

  let bus = null,
    route = null,
    updatePromises = [];
  var failedRecords = [];

  parsedRows.forEach(pr => {
    bus = allBuses.filter(b => b.number == pr.busNumber)[0];
    route = allRoutes.filter(r => r.name == pr.routeName)[0];
    if (!bus || !route) {
      failedRecords.push(pr);
      return;
    }

    updatePromises.push(Bus.findOneAndUpdate({ _id: bus._id }, { route: route._id }));
  });

  Promise.all(updatePromises).then(values => {
    res.json({
      successful: values,
      notFound: failedRecords
    });
  }).catch(err => {
    console.log(err);
    res.status(500).json(err);
  });
});

module.exports = router;
