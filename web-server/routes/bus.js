var express = require('express');
var router = express.Router();
var Bus = require('../models/bus'),
  RouteAssignmentHistory = require('../models/routeAssignmentHistory'),
  DeviceAssignmentHistory = require('../models/deviceAssignmentHistory');

//Get all buss
router.get('/', (req, res, next) => {
  Bus.find()
    .populate('route')
    .populate('device')
    .then(buses => {
      res.json(buses);
    }).catch(next);
});


router.post('/find', (req, res, next) => {
  let dataToFind = req.body;

  Bus.find(dataToFind).then(buses => {
    res.json(buses);
  }).catch(next);

});

//Create bus
router.post('/', (req, res, next) => {

  delete req.body._id;
  Bus.create(req.body).then(newBus => {

    if (newBus.route != null) {
      RouteAssignmentHistory.create({
        bus: newBus._id,
        route: newBus.route,
        user: req.body.user._id
      });
    }

    if (newBus.device != null) {
      DeviceAssignmentHistory.create({
        bus: newBus._id,
        device: newBus.device,
        user: req.body.user._id
      });
    }

    res.json(newBus);
  }).catch(next);
});

//Update bus
router.put('/', (req, res, next) => {
  let newBus = req.body;
  Bus.findOne({ _id: newBus._id }).then(previousData => {

    Bus.findByIdAndUpdate({ _id: newBus._id }, newBus, { new: true }).then((updatedBus) => {

      if (updatedBus.route != null && getValue(updatedBus.route) != getValue(previousData.route)) {
        RouteAssignmentHistory.create({
          bus: newBus._id,
          route: newBus.route,
          user: req.body.user._id
        });
      }

      if (updatedBus.device && getValue(updatedBus.device) != getValue(previousData.device)) {
        DeviceAssignmentHistory.create({
          bus: newBus._id,
          device: newBus.device,
          user: req.body.user._id
        });
      }

      res.json(updatedBus);
    });
  });
});

function getValue(id) {
  if (id == null || id == undefined)
    return null;
  else
    return id.toString();
}
//Delete bus
router.delete('/:id', (req, res, next) => {

  Bus.findOneAndDelete({ _id: req.params.id }).then(deletedBus => {
    res.json(deletedBus);
  }).catch(next);

});

router.post('/GetRouteAssignmentHistory', (req, res, next) => {
  let busId = req.body.busId;
  if (!busId) {
    res.status(422).json({ customErrorMessage: "No busId provided" });
    return;
  }

  RouteAssignmentHistory.find({ bus: busId })
    .populate('route')
    .populate('user')
    .sort([['date', 'descending']])
    .then(data => {
      res.json(data);
    }).catch(next);

});

router.post('/GetDeviceAssignmentHistory', (req, res, next) => {
  let busId = req.body.busId;
  if (!busId) {
    res.status(422).json({ customErrorMessage: "No busId provided" });
    return;
  }

  DeviceAssignmentHistory.find({ bus: busId })
    .populate('device')
    .populate('user')
    .sort([['date', 'descending']])
    .then(data => {
      res.json(data);
    }).catch(next);

});

module.exports = router;
