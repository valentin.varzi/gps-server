//Necessary imports
const express = require('express');
const app = express(),
  bodyParser = require('body-parser'),
  cors = require('cors'),
  routes = require('./routes'),
  db = require('./helpers/db'),
  errorHandler = require('./helpers/errorHandler'),
  AppSocket = require('./app-socket'),
  gpsSocket = require('../gps-socket/gps-socket'),
  positionLogger = require('./helpers/positionLogger'),
  consoleLogger = require('./helpers/consoleLogger');

//Creating http server
const http = require('http').Server(app);
app.use(cors());
app.use(bodyParser.json());


//Public endpoint
app.use('/public', express.static('uploads'));
//Declaring endpoints
routes.forEach(r => {
  app.use(r.path, r.module);
});
//Error handler
app.use(errorHandler);

//Initializing server
http.listen(3000, () => {

  //Database setup
  db.initialize();

  //Web-Mobile Apps socket set up
  var appSocket = AppSocket.initialize(http);

  // Sockets set up
  gpsSocket.initialize((dataFromDevice) => {
    // consoleLogger.log(dataFromDevice);
    positionLogger.log(dataFromDevice).then(loggedData => {
      appSocket.manageData(loggedData);
    }).catch(err => {
      console.log('logging error');
      console.log(err);
    });
  });

  //All done
  console.log(`Web Server listening on port 3000`);
});