const moment = require('moment');
var activeDevices = [];
var actualDate = null;

function log(rawNewData) {
  //add it
  var newData = JSON.parse(JSON.stringify(rawNewData));
  newData.date = new Date();
  if (activeDevices.filter(ad => ad.deviceId == newData.deviceId).length > 0) {
    activeDevices.filter(ad => ad.deviceId == newData.deviceId)[0].date = new Date();
  } else {
    activeDevices.push(newData);
  }

  //clear devices with 60s of inactivity
  actualDate = new Date();
  activeDevices = activeDevices.filter(ad => (actualDate.getTime() - ad.date.getTime()) / 1000 < 60);
  activeDevices.sort((a, b) => {
    if (a.deviceId > a.deviceId)
      return 1;
    else if (a.deviceId < a.deviceId)
      return -1;
    else
      return 0;
  })

  console.clear();
  console.log('::::::::::::::::::ACTIVE DEVICES::::::::::::::::::')
  activeDevices.forEach(ad => {
    console.log('device: ' + ad.deviceId + ', lastPing at ' + moment(ad.date).format('hh:mm:ss A') + ", " + ((actualDate.getTime() - ad.date.getTime()) / 1000).toFixed(2) + 's ago');
  });

  return activeDevices;
}

module.exports = {
  log: log
}