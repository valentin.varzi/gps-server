

function gpsLatToDecimal(pos) {
  var dg = parseInt(pos / 100);
  var minutes = pos - (dg * 100);
  var res = (minutes / 60) + dg;

  return parseFloat(res.toFixed(7));
}

function gpsLngToDecimal(pos) {
  var dg = parseInt(pos / 100);
  var minutes = pos - (dg * 100);
  var res = (minutes / 60) + dg;
  return parseFloat((res * -1).toFixed(7));
}

function latLngToGPS(lat, lng) {

  var toDegreesMinutesAndSeconds = (coordinate) => {
    var absolute = Math.abs(coordinate);
    var degrees = Math.floor(absolute);
    var minutesNotTruncated = (absolute - degrees) * 60;
    var minutes = Math.floor(minutesNotTruncated);
    var seconds = ((minutesNotTruncated - minutes) * 60).toFixed(4);

    return degrees + "" + minutes + "." + (seconds).split(".").join("");
  }

  return {
    latitude: toDegreesMinutesAndSeconds(lat),
    latitudeCardinal: lat >= 0 ? "N" : "S",
    longitude: toDegreesMinutesAndSeconds(lng),
    longitudeCardinal: lng >= 0 ? "E" : "W"
  };
}

function isInsideRoute(route, pos) {
  for (var c = false, i = -1, l = route.length, j = l - 1; ++i < l; j = i)
    ((route[i].lat <= pos.lat && pos.lat < route[j].lat) || (route[j].lat <= pos.lat && pos.lat < route[i].lat))
      && (pos.lng < (route[j].lng - route[i].lng) * (pos.lat - route[i].lat) / (route[j].lat - route[i].lat) + route[i].lng)
      && (c = !c);
  return c;
}

module.exports = {
  gpsLatToDecimal: gpsLatToDecimal,
  gpsLngToDecimal: gpsLngToDecimal,
  latLngToGPS: latLngToGPS,
  isInsideRoute: isInsideRoute
}