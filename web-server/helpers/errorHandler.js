module.exports = (err, req, res) => {
  if (!res.status)
    return;

  console.log('---------------error-----------------');
  console.log(err);
  console.log('-------------endError----------------');

  if (err.customErrorMessage) {
    console.log('viene por aqui');
    res.status(422).json({ message: err.customErrorMessage });
  }
  else if (err.name == "ValidationError")
    res.status(422).json({ message: err.errors[Object.keys(err.errors)[0]].message });
  else if (err.code == 11000)
    res.status(422).json({ message: 'Registro duplicado' })
  else
    res.status(422).json({ message: 'Los datos no pueden ser procesados' });
}