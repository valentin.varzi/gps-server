const mongoose = require('mongoose');

module.exports = {
  initialize: () => {
    mongoose.connect('mongodb://localhost/Atusum', {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true
    });

    mongoose.Promise = global.Promise;
  }
}
