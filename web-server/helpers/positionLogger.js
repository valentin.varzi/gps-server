const PositionHistory = require('../models/positionHistory'),
  Bus = require('../models/bus'),
  Device = require('../models/device'),
  cLab = require('../helpers/coordinatesLab');

function parseGPSDate(date, time) {
  //this will generate an UTC date
  if (!date)
    date = "";
  var actualDate = new Date();


  var year = parseInt(date.substring(4, 6));
  if (isNaN(year))
    year = actualDate.getUTCFullYear();
  else
    year = year + 2000;

  var month = parseInt(date.substring(2, 4));
  if (isNaN(month))
    month = actualDate.getUTCMonth();
  else
    month--;

  var day = parseInt(date.substring(0, 2));
  if (isNaN(day))
    day = actualDate.getUTCDate();

  var hours = parseInt(time.substring(0, 2));
  if (isNaN(hours))
    hours = actualDate.getUTCHours();
  var minutes = parseInt(time.substring(2, 4));
  if (isNaN(minutes))
    minutes = actualDate.getUTCMinutes();
  var seconds = parseInt(time.substring(4, 6));
  if (isNaN(seconds))
    seconds = actualDate.getUTCSeconds();

  return new Date(Date.UTC(year, month, day, hours, minutes, seconds));
}

module.exports = {
  log: (data) => {
    return new Promise((resolve, reject) => {

      Device.findOne({ deviceId: data.deviceId }).then((device) => {
        if (!device) {
          console.log(`deviceId: '${data.deviceId}' not found`);
          reject({ customErrorMessage: `deviceId: '${data.deviceId}' not found` });
        }
        return Bus.findOne({ 'device': device._id });
      }).then(bus => {
        if (!bus)
          bus = {};

        let newPosRecord = {
          bus: bus._id,
          route: bus.route,
          device: bus.device,
          lat: cLab.gpsLatToDecimal(data.latitude),
          lng: cLab.gpsLngToDecimal(data.longitude),
          speed: parseFloat(data.speed) || 0.0,
          direction: data.orientation,
          deviceId: data.deviceId,
          date: parseGPSDate(data.date, data.time),
          rawData: data
        };

        return PositionHistory.create(newPosRecord);
      }).then((posRecord) => {
        resolve(posRecord);
      }).catch(err => {
        reject(err);
      });

    });
  }
};