const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const RouteHistorySchema = new Schema({
  route: {
    type: ObjectId,
    ref: 'busRoute'
  },
  name: {
    type: String,
    required: [true, 'Campo nombre requerido']
  },
  active: {
    type: Boolean,
    default: true
  },
  wayPoints: {
    type: [{
      lat: { type: Number },
      lng: { type: Number }
    }],
    default: []
  },
  places: {
    type: [String],
    default: []
  },
  routeLength: {
    type: Number,
    default: 0
  },
  user: {
    type: ObjectId,
    ref: 'user'
  },
  date: { type: Date, default: Date.now }
}, {
  versionKey: false //To avoid _v on documents
});

const RouteHistory = mongoose.model('busRouteHistory', RouteHistorySchema, 'busRouteHistory');

module.exports = RouteHistory;