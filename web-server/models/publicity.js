const mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-beautiful-unique-validation'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const PublicitySchema = new Schema({
  title: {
    type: String,
    required: [true, 'Campo titulo es necesario']
  },
  description: { type: String },
  date: {
    type: Date,
    default: Date.now
  },
  imgName: {
    type: String
  },
  allRoutes: {
    type: Boolean
  },
  routes: [{
    type: ObjectId,
    ref: 'busRoute'
  }],
  active: {
    type: Boolean,
    default: false
  },
  activity: [{
    description: { type: String },
    user: {
      type: ObjectId,
      ref: 'user'
    },
    date: {
      type: Date,
      default: Date.now
    }
  }]
}, {
  versionKey: false //To avoid _v on documents
});
PublicitySchema.plugin(uniqueValidator);

const Publicity = mongoose.model('publicity', PublicitySchema, 'publicity');

module.exports = Publicity;
