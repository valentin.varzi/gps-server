const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;


const PositionHistorySchema = new Schema({
  bus: {
    type: ObjectId,
    ref: 'bus'
  },
  route: {
    type: ObjectId,
    ref: 'busRoute'
  },
  device: {
    type: ObjectId,
    ref: 'device'
  },
  lat: { type: Number },
  lng: { type: Number },
  speed: { type: Number },
  direction: { type: String },
  date: { type: Date },
  deviceId: { type: String },
  rawData: {
    type: {
      deviceId: { type: String },
      cmd: { type: String },
      effectiveData: { type: String },
      date: { type: Date },
      latitude: { type: Number },
      longitude: { type: Number },
      speed: { type: String },
      orientation: { type: String },
      hexString: { type: String }
    }
  }
}, {
  versionKey: false //To avoid _v on documents
});


const PositionHistory = mongoose.model('positionHistory', PositionHistorySchema, 'positionHistory');

module.exports = PositionHistory;