const mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-beautiful-unique-validation'),
  Schema = mongoose.Schema;

const PermissionSchema = new Schema({
  description: {
    type: String
  },
  path: {
    type: String
  }
}, {
  versionKey: false //To avoid _v on documents
});

const Permission = mongoose.model('permission', PermissionSchema, 'permission');

module.exports = Permission;
