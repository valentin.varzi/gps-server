const mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-beautiful-unique-validation'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const BusRouteSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Campo nombre requerido']
  },
  active: {
    type: Boolean,
    default: true
  },
  wayPoints: {
    type: [{
      lat: { type: Number },
      lng: { type: Number }
    }],
    default: []
  },
  places: {
    type: [String],
    default: []
  },
  routeLength: {
    type: Number,
    default: 0
  }
}, {
  versionKey: false //To avoid _v on documents
});

BusRouteSchema.plugin(uniqueValidator);

const BusRoute = mongoose.model('busRoute', BusRouteSchema, 'busRoute');

module.exports = BusRoute;