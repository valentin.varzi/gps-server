const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const DeviceNumberHistorySchema = new Schema({
  device: {
    type: ObjectId,
    ref: 'device'
  },
  phoneNumber: {
    type: Number
  },
  user: {
    type: ObjectId,
    ref: 'user'
  },
  date: { type: Date, default: Date.now }
}, {
  versionKey: false //To avoid _v on documents
});

const DeviceNumberHistory = mongoose.model('device', DeviceNumberHistorySchema, 'device');

module.exports = DeviceNumberHistory;