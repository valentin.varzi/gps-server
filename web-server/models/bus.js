const mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-beautiful-unique-validation'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const BusSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Campo nombre es requerido'],
    unique: 'Ya existe una unidad registrada con la descripción: \'{VALUE}\'',
  },
  number: {
    type: String,
    required: [true, 'Número de autobus requerido'],
    unique: 'Ya existe un autobus con este numero'
  },
  route: {
    type: ObjectId,
    ref: 'busRoute'
  },
  active: {
    type: Boolean,
    default: true
  },
  device: {
    type: ObjectId,
    ref: 'device'
  }
}, {
  versionKey: false //To avoid _v on documents
});
BusSchema.plugin(uniqueValidator);

const Bus = mongoose.model('bus', BusSchema, 'bus');

module.exports = Bus;
