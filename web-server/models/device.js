const mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-beautiful-unique-validation'),
  Schema = mongoose.Schema;

const DeviceSchema = new Schema({
  deviceId: {
    type: String,
    required: [true, 'Campo nombre es requerido'],
    unique: 'Ya existe una unidad registrada con la descripción: \'{VALUE}\'',
  },
  phoneNumber: {
    type: Number
  },
  active: {
    type: Boolean,
    default: true
  },
  description: {
    type: String
  }
}, {
  versionKey: false //To avoid _v on documents
});
DeviceSchema.plugin(uniqueValidator);

const Device = mongoose.model('device', DeviceSchema, 'device');

module.exports = Device;