const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

const UserSchema = new Schema({
  username: {
    type: String,
    required: [true, 'Campo usuario es requerido']
  },
  name: {
    type: String,
    required: [true, 'Se debe asignar un nombre al usuario']
  },
  email: {
    type: String,
    required: [true, 'Campo correo es requerido'],
    validate: {
      validator: v => re.test(v),
      message: props => `Ingrese un correo valido`
    }
  },
  password: {
    type: String,
    required: [true, 'Campo contraseña es requerido'],
    validate: {
      validator: v => v.length >= 5,
      message: props => `Ingrese una contraseña de 6 o mas caracteres`
    }
  },
  isAdmin: {
    type: Boolean,
    default: false
  },
  active: {
    type: Boolean,
    default: true
  },
  permissions: [{
    permission: {
      type: ObjectId,
      ref: 'permission'
    },
    allowed: Boolean
  }]
}, {
  versionKey: false //To avoid _v on documents
});

const User = mongoose.model('user', UserSchema, 'user');

module.exports = User;
