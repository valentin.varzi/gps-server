const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const RouteAssignmentHistorySchema = new Schema({
  bus: {
    type: ObjectId,
    ref: 'bus'
  },
  route: {
    type: ObjectId,
    ref: 'busRoute'
  },
  user: {
    type: ObjectId,
    ref: 'user'
  },
  date: { type: Date, default: Date.now }
}, {
  versionKey: false //To avoid _v on documents
});


const RouteAssignmentHistory = mongoose.model('routeAssignmentHistory', RouteAssignmentHistorySchema, 'routeAssignmentHistory');

module.exports = RouteAssignmentHistory;