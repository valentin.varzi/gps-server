const mongoose = require('mongoose'),
  uniqueValidator = require('mongoose-beautiful-unique-validation'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;

const ReportSchema = new Schema({
  title: {
    type: String,
  },
  date: {
    type: Date,
    default: Date.now
  },
  tickerNumber: {
    type: String
  },
  imgName: {
    type: String
  },
  bus: {
    type: ObjectId,
    ref: 'bus'
  },
  route: {
    type: ObjectId,
    ref: 'busRoute'
  },
  phoneId: {
    type: String,
    required: [true, 'Campo phoneId es necesario']
  },
  contactPhoneNumber: {
    type: String
  },
  description: { type: String },
  response: {
    text: {
      type: String
    },
    date: {
      type: Date
    }
  }
}, {
  versionKey: false //To avoid _v on documents
});
ReportSchema.plugin(uniqueValidator);

const Report = mongoose.model('report', ReportSchema, 'report');

module.exports = Report;
