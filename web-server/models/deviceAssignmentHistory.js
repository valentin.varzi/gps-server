const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  ObjectId = Schema.ObjectId;


const DeviceAssignmentHistorySchema = new Schema({
  bus: {
    type: ObjectId,
    ref: 'bus'
  },
  device: {
    type: ObjectId,
    ref: 'device'
  },
  user: {
    type: ObjectId,
    ref: 'user'
  },
  date: { type: Date, default: Date.now }
}, {
  versionKey: false //To avoid _v on documents
});


const DeviceAssignmentHistory = mongoose.model('deviceAssignmentHistory', DeviceAssignmentHistorySchema, 'deviceAssignmentHistory');

module.exports = DeviceAssignmentHistory;