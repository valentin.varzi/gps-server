const mongoose = require('mongoose'),
  Schema = mongoose.Schema;

const LogsSchema = new Schema({
  type: {
    type: String
  },
  message: {
    type: String
  },
  ref: {
    type: String
  },
  date: { type: Date, default: Date.now }
}, {
  versionKey: false //To avoid _v on documents
});

const Logs = mongoose.model('log', LogsSchema, 'log');

module.exports = Logs;