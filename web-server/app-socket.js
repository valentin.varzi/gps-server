const io = require('socket.io')();

const AppSocket = function (httpServer) {

  io.attach(httpServer, {
    cors: { origin: '*' }
  });

  this.onConnection = (socket) => {
    socket.on('quitRoom', (data, callback) => { this.removeSocketFromRoom(socket, data, callback); });
    socket.on('subscribeToAllData', (data) => { this.subscribeToAllData(socket, data) });
    socket.on('subscribeToRoute', (data, callback) => { this.subscribeToRoute(socket, data, callback); });
    socket.on('subscribeToBus', (data, callback) => { this.subscribeToBus(socket, data, callback) });
    socket.on('disconnect', this.onDisconnection);
  };

  this.removeSocketFromRoom = (socket, data, callback) => {
    if (data.room == 'all') {
      var socketRooms = Object.keys(socket.rooms);
      socketRooms.forEach(r => {
        if (r.indexOf("route/") >= 0 || r.indexOf("bus/") >= 0)
          socket.leave(r);
      });
    }
    else
      socket.leave(data.room);

    if (typeof callback == "function")
      callback(true);
  }

  this.subscribeToRoute = (socket, data, callback) => {
    if (!data || !data.routeId)
      return;

    socket.join('route/' + data.routeId);
    if (callback)
      callback('Subscribed to \'route/' + data.routeId + '\'');
  };

  this.subscribeToBus = (socket, data, callback) => {
    if (!data || !data.busId)
      return;

    socket.join('bus/' + data.busId);
    if (callback)
      callback('Subscribed to \'bus/' + data.busId + '\'');
  };

  this.subscribeToAllData = (socket) => {
    socket.join('allData');
  };

  this.onDisconnection = () => {
  };


  this.manageData = (data) => {
    if (data.route && io.sockets.adapter.rooms['route/' + data.route])
      io.to('route/' + data.route).emit('route', data);

    if (data.bus && io.sockets.adapter.rooms['bus/' + data.bus])
      io.to('bus/' + data.bus).emit('bus', data);

    if (io.sockets.adapter.rooms['allData'])
      io.to('allData').emit('allData', data);
  }

  this.emitToRoute = (data) => {
    io.to(data.routeId).emit(data);
  }

  io.on('connection', this.onConnection);
  console.log('App Socket Initialized on port 3000');
  return this;
};

module.exports = {
  initialize: (httpServer) => AppSocket(httpServer)
};