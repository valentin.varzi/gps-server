exports.protocol = "ST901";
exports.model_name = "ST901";
exports.compatible_hardware = ["ST901/supplier"];

var adapter = function (device) {
  if (!(this instanceof adapter))
    return new adapter(device);

  this.format = {
    start: '*',
    end: '#',
    separator: ','
  };
  this.device = device;
  this.__count = 1;


  this.parse_data = function (buffer) {

    var hexString = this.parseBufferToString(buffer);

    var parts = hexString.split(",");
    //*HQ,917711771589,V1,200033,V,2546.3153,N,10858.7200,W,000.00,000,010119,FBFFBBFF,334,02,7818,24968#

    return {
      nameOfMaker: parts[0],
      device_id: parts[1],
      cmd: parts[2], action: 'ping',
      time: parts[3],
      effectiveData: parts[4] == 'S',
      latitude: parts[5],
      latitudeMark: parts[6],
      longitude: parts[7],
      longitudeMark: parts[8],
      speed: parts[9] == "" ? 0 : parts[9],
      direction: parts[10] == "" ? 0 : parts[10],
      date: parts[11],
      vehicleStatus: parts[12],
      hexString: hexString
    };

  };

  this.get_ping_data = function (data) {
    // var str = msg_parts.data;

    var data = {
      deviceId: data.device_id,
      cmd: data.cmd,
      effectiveData: data.effectiveData,
      date: data.date,
      time: data.time,
      latitude: data.latitude,
      longitude: data.longitude,
      speed: data.speed,
      orientation: data.direction,
      hexString: data.hexString
    };

    return data;
  }

  this.request_login_to_device = function () {
    //@TODO: Implement this.
  };
  this.authorize = function (data) {
    this.send_comand(['HQ', data.device_id, 'V4', 'V1', this.formatDate(new Date())]);
  };

  this.dex_to_degrees = function (dex) {
    return parseInt(dex, 16) / 1800000;
  };

  this.send_comand = function (cmd, data) {
    var msg = [cmd, data];
    this.device.send(this.format_data(msg));
  };
  this.format_data = function (params) {
    /* FORMAT THE DATA TO BE SENT */
    var str = this.format.start;
    if (typeof (params) == 'string') {
      str += params;
    } else if (params instanceof Array) {
      str += params.join(this.format.separator);
      str = str.slice(0, -1);
    } else {
      throw 'The parameters to send to the device has to be a string or an array';
    }
    str += this.format.end;
    return str;
  };

  this.parseBufferToString = function (buffer) {
    var result = this.bufferToHexString(buffer);
    return this.hexToASCII(result);
  };

  this.bufferToHexString = function (buffer) {
    var str = '';
    for (var i = 0; i < buffer.length; i++) {
      if (buffer[i] < 16) {
        str += '0';
      }
      str += buffer[i].toString(16);
    }
    return str;
  };

  this.hexToASCII = function (hexx) {
    var hex = hexx.toString();//force conversion
    var str = '';
    for (var i = 0; (i < hex.length && hex.substr(i, 2) !== '00'); i += 2)
      str += String.fromCharCode(parseInt(hex.substr(i, 2), 16));
    return str;
  }

  this.formatDate = function (date) {
    if (typeof date.getMonth != 'function')
      date = new Date();

    var year = date.getFullYear();
    var month = date.getMonth();
    month = month > 9 ? month : "0" + month;
    var day = date.getDate();
    day = day > 9 ? day : "0" + day;
    var hours = date.getDate();
    hours = hours > 9 ? hours : "0" + hours;
    var minutes = date.getMinutes();
    minutes = minutes > 9 ? minutes : "0" + minutes;
    var seconds = date.getSeconds();
    seconds = seconds > 9 ? seconds : "0" + seconds;

    return year + "" + month + "" + day + "" + hours + "" + minutes + "" + seconds;
  };
}
exports.adapter = adapter;