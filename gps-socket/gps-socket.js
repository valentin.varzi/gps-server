var gps = require("./gps-tracking/index.js");

const options = {
  'debug': true,
  'port': 3001,
  'device_adapter': "ST901"
}

const socketManager = {
  server: null,
  initialize: (logDataFunction) => {
    this.server = gps.server(options, (device, connection) => {

      device.on("login_request", function (device_id, msg_parts) {
        this.login_authorized(true, msg_parts);
      });

      device.on("ping", function (data, other) {
        logDataFunction(data);
      });

    });

    console.log('GPS Socket Initialized on port ' + options.port);
  }
}
// socketManager.startServer();
//*HQ,917711771589,V1,000033,V,2546.3153,N,10858.7200,W,000.00,000,010119,FBFFBBFF,334,02,7818,24968#
module.exports = socketManager;