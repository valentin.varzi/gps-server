# Web Server / APIs

  ##  CRUD Apis
  * Usuarios
  * Rutas
  * Autobus

  ##  Seguridad
  * Login
  - Backup Automaticos
  - Descarga archivo backup

  ##  Historial y registros
  - API lectura de historial con filtros:
    - Autobus
    - Dispositivo
    - Ruta
    - Fecha y Hora
  - Autobus-Dispositivo
  - Autobus-Ruta
  - Conexiones-Desconexiones
  - Autobus Fuera de ruta

  ##  Publicidad
  - Registro y carga de multimedia
  - Activar/Desactivar

  ##  Socket
  - Dispositivo GPS
    - Recepcion de datos de dispositivos
      - Parseo de datos
    - Registro de conexiones - desconexiones
    - Registro de ubicacion
    - Deteccion salio de ruta
  - Admin Web / Aplicacion
    - Stream por autobus
    - Stream por ruta
    - Stream todos
    - Autobuses activos

# Admin Web

  ##  Pagina Login

  ##  Conectividad
  - Validacion servidor no responde
  - Reconexión automatica

  ##  Menu
  - Pintado normal
  - Pintado dinamico por permisos

  ##  Dashboard
  - Autobuses activos / Autobuses conectados
  * Rutas activas
  - Usuarios de app hoy/semana/mes
  - Visualizacion de autobuses en mapa
  - Filtro por autobus
  - Filtro por ruta
  - Pintado de ruta

  ##  CRUD Usuarios
  - Permisos por usuario

  ##  CRUD Autobuses
  * Asignacion de dispositivo
  * Historial de dispositivos asignados

  ##  Historial
  - Recorridos
    - Filtro de fecha
    - Filtro por ruta
    - Filtro por autobus
  - Uso de app
    - Filtro de fecha
    - Filtro por ruta
    - Filtro por autobus

# Applicacion

  ##  Diseño
  - Pantalla de carga/bienvenida
  - Menu
  - Pantalla principal con mapa
  - Selector de rutas
  - Visualizador de publicidad
  - Iconos
  - Notificaciones

  ##  Conectividad
  - Exitosa
  - Sin conexion
  - Servidor no responde
  - Servidor fuera de servicio
  - Desconexiones
  - Reconexiones automaticas

  ##  Geolocalización
  - Obtencion de ubicacion
  - Validacion de ubicacion en los mochis
  - Validacion de no se obtiene ubicacion

  ##  Mapas
  - Pintado de mapa
  - Pintado de ubicacion actual
  - Pintado de ruta
  - Pintado de autobuses
  - Seleccion de autobus
  - filtrados por ruta o seleccionado
  - Estimacion de tiempo de encontrar autobus
    - Validar que se pueda calcular
  - Estimacion de tiempo de llegar a destino

  ##  Rutas
  - Carga de rutas
  - Selector de rutas por listado
  - Selector de rutas por destino final

  ##  Registro de uso
  - Registro inicial
  - Aplicacion abierta
  - Rutas buscadas
  - Lugares buscados

  ##  Publicidad
  - Carga de publicidad
  - Visualizacion de publicidad

  ##  Notificaciones
  - Receptor de notificaciones
  - Eventos de notificaciones
